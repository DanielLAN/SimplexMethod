/*
**Big_M Method with Revised Tableau for standard form Linear Programing and general form.

**Formulation:
**Minimize C'X
**Subject to AX=b
**           X>=0
** 	OR
**Minimize C'X
**Subject to AX<=b
**           X>=0
**Author: Danny
** 上海财经大学信管学院16级实验班 2015111547 兰程昊
*/
#include<cstdio>
#include<cstring>
#include<math.h>

struct BIGM;
const int pro =13;//protection防止溢出，不用改动
const int width = 15;//表示每个单元格的宽度，如果出现打印混乱，适当扩大width
int gomax;
double **A,*b,*X;
BIGM *C;
bool *Inbasis;
int *Inverse,*BASIS,*InverseSub;
    FILE* out;

int initialize(int m,int n);
int Iterate(int m,int n,int length,int L);
int StandardInputDATA(int m,int n,int &length,FILE* input);
int GeneralInputDATA(int m,int &n,int &length,FILE* input);
BIGM getCOST(int m,int n);
BIGM getREDUCEDCOST(int aimj,double *Aj,int m,int n);
void changeA(int m,int n,int length);
void changeBASIS(int m,int rei,int rej,double *Aj,BIGM r);
void outputtalbleau(int m,int n,int length,int L);
void printstep(int step,int m,int n,int linelen,int L);
void GoGeneral(FILE* in);
void GoStandard(FILE* in);
int main()
{
	gomax = 1;//if you want to minimize, change it to 0;

	FILE *in;
	in = fopen("In4","r");			//the path of data used to test
	if(!in)printf("file not found\n");

	out = fopen("out.txt","w");

	GoGeneral(in);
	//GoStandard(in);

	return 0;
}
void GoStandard(FILE*in)
{
    int n,m;
    fscanf(in,"%d%d",&n,&m);
    int length=n;
    initialize(m,n+m);
    StandardInputDATA(m,n,length,in);
    Iterate(m,n,length,width);
}
void GoGeneral(FILE* in)
{
    int n,m;
    fscanf(in,"%d%d",&n,&m);
printf("%d",n);
    int length=n;
    initialize(m,n+m);
    GeneralInputDATA(m,n,length,in);
    Iterate(m,n,length,width);
}
struct BIGM
{
    double mc;
    double co;
    BIGM():mc(0),co(0){}
    BIGM(double _co):mc(0),co(_co){}
    BIGM(double _mc,double _co):mc(_mc),co(_co){}
    void print()
    {   double d;int m;int n;
        if(mc==0){d=co; m=int(d); n=int(abs((d-m)*1000));
            fprintf(out,"%2d.%2d",m,n);
        }
        else{d=mc; m=int(d); n=int(abs((d-m)*100));
            fprintf(out,"%2d.%2dM",m,n);
            d=co; m=int(d); n=int(abs((d-m)*100));
            fprintf(out,"%+2d.%2d",m,n);
        }
    }
    friend int abs(int a){return a>=0?a:-a;}
    friend double abs(double d){return d<0?-d:d;}
    friend bool operator < (const BIGM r1,const BIGM r2)
    {
        if(r1.mc != r2.mc)return r1.mc < r2.mc;
        return r1.co<r2.co;
    }
    friend bool operator < (const BIGM r1,const double a)
    {
        if(r1.mc == 0)return r1.co < a;
        return r1.mc<0;
    }
    friend bool operator > (const BIGM r1,const BIGM r2)
    {
        if(r1.mc != r2.mc)return r1.mc > r2.mc;
        return r1.co>r2.co;
    }
    friend bool operator > (const BIGM r1,const double a)
    {
        if(r1.mc == 0)return r1.co > a;
        return r1.mc>0;
    }
    friend BIGM operator *(BIGM r,double a)
    {
        r.mc *= a;
        r.co *= a;
        return r;
    }
    friend BIGM operator *(double a,BIGM r)
    {
        r.mc *= a;
        r.co *= a;
        return r;
    }
    friend BIGM operator -(BIGM r1,BIGM r2)
    {
        r1.mc -= r2.mc;
        r1.co -= r2.co;
        return r1;
    }
    friend BIGM operator +(BIGM r1,BIGM r2)
    {
        r1.mc += r2.mc;
        r1.co += r2.co;
        return r1;
    }
};

int length(int a){return a==0? 1 :int(log10(abs(a)))+1;}
double cut(double d){
    return double(int(d*100))/100;
    /*将DOUBLE类型变为只有两位小数（直接切）,负DOUBLE被强制转换位整形取上界*/
}
void printline(int length)
{
    fprintf(out,"\n");
    while(length--)fprintf(out,"-");
    fprintf(out,"\n");
}

void printcentre(int a,int L)
{
    int l=length(a);
    if(a<0)l++;
    int dif = L-l;int mid=dif/2;
    if(dif>0)for(int i=0;i<mid;i++)fprintf(out," ");
    fprintf(out,"%d",a);
    if(dif>0)for(int i=mid;i<dif;i++)fprintf(out," ");
    fprintf(out,"|");
}
void printcentre(double d,int L)
{
    d=cut(d);
    int m=int(d);
    int n=int(abs((d-m)*100));
    if(n==0)return printcentre(m,L);

    int l=length(m)+3;
    if(d<0)l++;
    int dif = L-l;int mid=(dif)/2;
    if(dif>0)for(int i=0;i<mid;i++)fprintf(out," ");
    fprintf(out,"%.2f",d);
    if(dif>0)for(int i=mid;i<dif;i++)fprintf(out," ");
    fprintf(out,"|");
}
void printcentre_X(int a,int L)
{
    int l=length(a)+1;
    if(a<0)l++;
    int dif = L-l;int mid=(dif+1)/2;
    if(dif>0)for(int i=0;i<mid;i++)fprintf(out," ");
    fprintf(out,"X%d",a);
    if(dif>0)for(int i=mid;i<dif;i++)fprintf(out," ");
    fprintf(out,"|");
}
void printcentre(char*s,int l,int L)
{
    int dif = L-l;int mid=(dif)/2;
    if(dif>0)for(int i=0;i<mid;i++)fprintf(out," ");
    for(int i=0;i<l;i++)fprintf(out,"%c",s[i]);
    if(dif>0)for(int i=mid;i<dif;i++)fprintf(out," ");
    fprintf(out,"|");
}
void printcentre(BIGM r,int L)
{
    double d1=cut(r.mc);double d2=cut(r.co);
    if(d1==0)return printcentre(d2,L);
    if(d2==0)
    {
        int m=int(d1);int n=int(abs((d1-m)*100));
        int l = length(m)+1;
        if(n)l+=3;if(d1<0)l++;if(abs(m)==1&&!n)l--;
        int dif = L-l;int mid=(dif+1)/2;
        if(dif>0)for(int i=0;i<mid;i++)fprintf(out," ");
        if(n)fprintf(out,"%.2fM",d1);
        else if(abs(m)==1)fprintf(out,m>0?"M":"-M");
        else fprintf(out,"%dM",m);
        if(dif>0)for(int i=mid;i<dif;i++)fprintf(out," ");
        fprintf(out,"|");
        return;
    }
    int m=int(d1);int n=int(abs((d1-m)*100));
    int m2=int(d2);int n2=int(abs((d2-m)*100));
    int l=length(m)+length(m2)+2;
    if(n)l+=3;if(d1<0)l++;if(n2)l+=3;if(abs(m)==1&&n==0)l--;
    int dif = L-l;int mid=dif/2;
    if(dif>0)for(int i=0;i<mid;i++)fprintf(out," ");
    if(n){
        if(n2)fprintf(out,"%.2fM%+.2f",d1,d2);
        else fprintf(out,"%.2fM%+d",d1,m2);
    }
    else if(abs(m)==1)
    {
        if(n2)fprintf(out,m>0?"M%+.2f":"-M%+.2f",d2);
        else fprintf(out,m>0?"M%+d":"-M%+d",m2);
    }
    else{
        if(n2)fprintf(out,"%dM%+.2f",m,d2);
        else fprintf(out,"%dM%+d",m,m2);
    }
    if(dif>0)for(int i=mid;i<dif;i++)fprintf(out," ");
    fprintf(out,"|");
}
void changeA(int m,int n,int length)
{
    if(m+n-length<length*0.05)return;

    double **AA;
    AA = new double*[m+pro];
    for(int i=1;i<=m;i++)
    {
        AA[i] = new double [length+pro];
        for(int j=1;j<=length;j++)AA[i][j] = A[i][j];
        delete[] A[i];
        //fprintf(out,"here\n");
        A[i]=AA[i];
    }
    A = AA;
}
int GeneralInputDATA(int m,int &n,int &length,FILE* input)
{
    int *Count,s=1;
    Count  = new int[n+pro];
    memset(Count,0,sizeof(int)*(n+pro));

    memset(Inverse,0,sizeof(int)*(m+pro));
    memset(InverseSub,0,sizeof(int)*(n+pro));
    memset(BASIS,0,sizeof(int)*(m+pro));
    memset(Inbasis,0,sizeof(int)*(m+n+pro));

    for(int j=1;j<=n;j++)fscanf(input,"%lf",&C[j].co);
    if(gomax)for(int j=1;j<=n;j++)C[j].co = -C[j].co;
    for(int j=n+1;j<=n+m;j++)C[j].co = 0;
    int orign = n;
    n+=m;
    for(int j=n+1;j<=n+m;j++)C[j].mc = 1;
    for(int i=1;i<=m;i++)
    {
        for(int j=1;j<=orign;j++)
        {
            fscanf(input,"%lf",&A[i][j]);
            if(A[i][j] == 0)Count[j]++;
        }
        for(int j=1;j<=m;j++)A[i][j+orign]=(j==i?1:0);
        fscanf(input,"%lf",&b[i]);
        if(b[i]<0)
        {
            b[i] = -b[i];
            for(int k=1;k<=n;k++)A[i][k] = -A[i][k];
        }else
        {
            BASIS[i] = Inverse[i] = i+orign;
            InverseSub[i+orign] = i;
            Inbasis[i+orign] = true;
            s++;
        }
    }
    //for(int j=orign+1;j<=n;j++)Count[j]=m-1;
    for(int j=1;j<=n && s<=m;j++)
    {
        if(Count[j]==m-1 && !Inbasis[j])
        {
            int i;
            for(i=1;i<=m;i++)if(A[i][j]!=0)break;
            if(A[i][j]<0 || Inverse[i])continue;
            if(A[i][j]!=1)
            {   double tem = A[i][j];
                for(int k=1;k<=n;k++)A[i][k] /= tem;
                b[i] /= tem;
            }
            BASIS[i] = Inverse[i] = j;
            InverseSub[j] = i;
            Inbasis[j] = true;
            s++;
        }
    }
    length = n;
    for(int k=1,i=1;s<=m;i++)
        if(!Inverse[i])
        {
            length = BASIS[i] = Inverse[i]=n+k;
            InverseSub[Inverse[i]] = i;
            Inbasis[Inverse[i]] = true;
            k++;
            s++;
            for(int ii=1;ii<=m;ii++)A[ii][Inverse[i]] = (ii==i) ? 1 :0;
        }
    //changeA(m,n,length);

    return 1;
}
int StandardInputDATA(int m,int n,int &length,FILE* input)
{
    int *Count,s=1;
    Count  = new int[n+pro];
    memset(Count,0,sizeof(int)*(n+pro));

    memset(Inverse,0,sizeof(int)*(m+pro));
    memset(InverseSub,0,sizeof(int)*(n+pro));
    memset(BASIS,0,sizeof(int)*(m+pro));
    memset(Inbasis,0,sizeof(int)*(m+n+pro));

    for(int j=1;j<=n;j++)fscanf(input,"%lf",&C[j].co);
    if(gomax)for(int j=1;j<=n;j++)C[j].co = -C[j].co;
    for(int j=n+1;j<=n+m;j++)C[j].mc = 1;
    for(int i=1;i<=m;i++)
    {
        for(int j=1;j<=n;j++)
        {
            fscanf(input,"%lf",&A[i][j]);
            if(A[i][j] == 0)Count[j]++;
        }
        fscanf(input,"%lf",&b[i]);
        if(b[i]<0)
        {
            b[i] = -b[i];
            for(int k=1;k<=n;k++)A[i][k] = -A[i][k];
        }
    }

    for(int j=1;j<=n && s<=m;j++)
    {
        if(Count[j]==m-1)
        {
            int i;
            for(i=1;i<=m;i++)if(A[i][j]!=0)break;
            if(A[i][j]<0 || Inverse[i])continue;
            if(A[i][j]!=1)
            {   double tem = A[i][j];
                for(int k=1;k<=n;k++)A[i][k] /= tem;
                b[i] /= tem;
            }
            BASIS[i] = Inverse[i] = j;
            InverseSub[j] = i;
            Inbasis[j] = true;
            s++;
        }
    }
    length = n;
    for(int k=1,i=1;s<=m;i++)
        if(!Inverse[i])
        {
            length = BASIS[i] = Inverse[i]=n+k;
            InverseSub[Inverse[i]] = i;
            Inbasis[Inverse[i]] = true;
            k++;
            s++;
            for(int ii=1;ii<=m;ii++)A[ii][Inverse[i]] = (ii==i) ? 1 :0;
        }
    changeA(m,n,length);
    return 1;
}
BIGM getCOST(int m,int n)
{
    BIGM r,tem;
    for(int i=1;i<=m;i++)
    {
        tem = C[BASIS[i]];
        r = r + tem * b[i];
    }
    return r;
}
BIGM getREDUCEDCOST(int aimj,double *Aj,int m,int n)
{
    if(InverseSub[aimj]>0)
        for(int i=1;i<=m;i++) Aj[i] = A[i][aimj];
    else for(int i=1;i<=m;i++)
    {
        Aj[i] = 0;
        for(int j=1;j<=m;j++)
        {
            Aj[i] += A[i][Inverse[j]] * A[j][aimj];
        }

    }
    BIGM r,tem;
    for(int i=1;i<=m;i++)
    {
        tem = C[BASIS[i]];
        r = r + tem * Aj[i];
    }
    tem = C[aimj];
    tem = tem - r;
    return tem;
}
void changeBASIS(int m,int rei,int rej,double *Aj,BIGM r)
{
    double key = Aj[rei];
    for(int j=1;j<=m;j++)A[rei][Inverse[j]] /= key;
    b[rei] /= key;
    for(int i=1;i<=m;i++)
    {
        if(i==rei)continue;
        key = Aj[i];
        b[i] -= b[rei]*key;
        for(int j=1;j<=m;j++)
            A[i][Inverse[j]] -= A[rei][Inverse[j]]*key;
    }
    Inbasis[BASIS[rei]] = 0;
    BASIS[rei] = rej;
    Inbasis[rej] = 1;
}

void printstep(int step,int m,int n,int linelen,int L)
{
    char s1[10]="Step",s2[10]="Cost";
    printline(linelen);
    printcentre(s1,4,L);
    printcentre(s2,4,L);
    for(int i=1;i<=m;i++)printcentre_X(BASIS[i],L);
    printline(linelen);
    printcentre(step,L);
    printcentre(gomax?-1*getCOST(m,n):getCOST(m,n),L);
    for(int i=1;i<=m;i++)printcentre(b[i],L);
    fprintf(out,"\n");
}
void outputtalbleau(int m,int n,int length,int L)
{
    //this method will change the matrix A entirely，so I don't suggest you to use it during iteration.
    double *Aj;
    Aj = new double[m+pro];
    for(int j=1;j<=length;j++)printcentre_X(j,L);
    printline((length+0.8)*L);
    for(int j = 1;j<=length;j++)printcentre(gomax ? -1*C[j]:C[j],L);
    char s[10]="__C/Xb__";
    printcentre(s,8,L);
    fprintf(out,"\n");
    for(int j=1;j<=length;j++)
    {
        if(!Inbasis[j])
        {

            BIGM tem = getREDUCEDCOST(j,Aj,m,n);
            if(gomax)tem = tem*-1;
             /* //check Aj
            fprintf(out,"\n");
            for(int i=1;i<=m;i++)fprintf(out,"%f ",Aj[i]);
            fprintf(out,"\n");*/
            for(int i=1;i<=m;i++) A[i][j] = Aj[i];//if you wanna use this method during iteration , please cut this part.
            printcentre(tem,L);

        }
        else printcentre(0,L);
    }
    printcentre(gomax ? getCOST(m,n):getCOST(m,n)*-1,L);
    printline((length+1.8)*L);
    for(int i=1;i<=m;i++)
    {
        for(int j=1;j<=length;j++)printcentre(A[i][j],L);
        printcentre(b[i],L);
        fprintf(out,"\n");
    }
}

int Iterate(int m,int n,int length,int L)
{
    fprintf(out,"\nBeginning Tableau:\n");
    outputtalbleau(m,n,length,L);
    double *Aj;
    Aj = new double[m+pro];
    for(int iteration=1;;iteration++)
    {
        bool enter = false;
        for(int j=1;j<=length;j++)
        {
            if(!Inbasis[j])
            {
                BIGM tem = getREDUCEDCOST(j,Aj,m,n);
                if(tem < 0)
                {
                    bool infinite = true;
                    enter = true;
                    int rei=-1;
                    double theta = 1<<29;
                    for(int i=1;i<=m;i++)
                    {
                        if(Aj[i] >0)
                        {
                            infinite = false;
                            if(b[i]/Aj[i] < theta){rei = i;theta = b[i]/Aj[i];}
                            else if(b[i]/Aj[i]==theta)
                            {
                                if( rei ==-1 || b[i]/Aj[i] > b[rei]/Aj[rei])rei = i;
                                else for(int k = length;k>=1;k--)
                                        if(A[i][k]/Aj[i] > A[rei][k]/Aj[rei])rei = i;
                            }
                        }
                    }
                    if(infinite)
                    {
                        fprintf(out,"\n\nUnbounded Tableau:\n");
                        outputtalbleau(m,n,length,L);
                        fprintf(out,"\nUNBOUNED!!\n");
                        return 1;
                    }
                    changeBASIS(m,rei,j,Aj,tem);
                    break;
                }
            }
        }
        if(!enter)break;
        printstep(iteration,m,n,(m+2.5)*L,L);
    }
    fprintf(out,"\n\nEND Tableau:\n");
    outputtalbleau(m,n,length,L);
    BIGM optimal = gomax?-1*getCOST(m,n):getCOST(m,n);
    if(optimal.mc != 0)
    {
        fprintf(out,"\nInfeasible!!");
        return -1;
    }
    fprintf(out,"\n\nThis LP problem has an optimal value\t%f\nBest solution is:\tX[",optimal.co);
    memset(X,0,sizeof(double)*(n+pro));
    for(int i=1;i<=m;i++)X[BASIS[i]] = b[i];
    for(int j=1;j<=n;j++)fprintf(out," %.3f ",X[j]);
    fprintf(out,"]\n");
    return 0;
}
int initialize(int m,int n)
{
    n+=pro;
    A = new double*[m+pro];
    for(int i=1;i<=m;i++)
        A[i] = new double [m+n];
    m+=pro;
    b = new double [m];
    C = new BIGM [n];
    X = new double [n];
    Inbasis = new bool[m+n];
    BASIS = new int [m];
    Inverse = new int [m];
    InverseSub = new int [n];
    return 1;
}
